import { Inject, Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/services/users/users.service';
import { comparePassword } from 'src/utils/bcrypt';

@Injectable()
export class AuthService {
  constructor(
    @Inject('USER_SERVICE') private readonly userService: UsersService,
  ) {}

  async validateUser(username: string, password: string) {
    const userInDB = await this.userService.findUserByUsername(username);
    if (userInDB) {
      const isVerified = comparePassword(password, userInDB.password);
      if (isVerified) {
        return userInDB;
      }
    }
    console.log('Verification failed');
    return null;
  }
}
