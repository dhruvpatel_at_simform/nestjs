import { NestMiddleware } from '@nestjs/common';
import { NextFunction, Response, Request } from 'express';

export class ValidateCustomerMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    console.log('hello from validateCustomerMiddleware');
    next();
  }
}
