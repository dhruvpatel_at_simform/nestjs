import {
  IsEmail,
  IsInt,
  IsNotEmpty,
  Length,
  ValidateNested,
  IsNotEmptyObject,
} from 'class-validator';
import { Type } from 'class-transformer';
import { CreateAddressDto } from './CreateAddress.dto';

export class CreateCustomerDto {
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsInt()
  @IsNotEmpty()
  id: number;

  @Length(5, 15)
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  createdAt: string;

  @IsNotEmptyObject()
  @ValidateNested()
  @Type(() => CreateAddressDto)
  address: CreateAddressDto;
}
