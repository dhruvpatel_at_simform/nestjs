export interface Customer {
  email: string;
  id: number;
  name: string;
  createdAt: string;
}
