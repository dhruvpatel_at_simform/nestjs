import { Injectable } from '@nestjs/common';
import { CreateCustomerDto } from 'src/customers/dtos/CreateCustomer.dto';
import { Customer } from 'src/customers/types/Customers';

@Injectable()
export class CustomersService {
  private customers: Array<Customer> = [
    {
      name: 'John',
      email: 'john@gmail.com',
      createdAt: new Date().toString(),
      id: 1,
    },
    {
      name: 'Jane',
      email: 'Jane@gmail.com',
      createdAt: new Date().toString(),
      id: 2,
    },
    {
      name: 'Jina',
      email: 'Jina@gmail.com',
      createdAt: new Date().toString(),
      id: 3,
    },
  ];

  findCustomerById(id: Number) {
    return this.customers.find((user) => user.id === id);
  }

  createCustomer(createCustomerDto: CreateCustomerDto) {
    this.customers.push(createCustomerDto);
    return createCustomerDto;
  }

  getCustomers() {
    return this.customers;
  }
}
