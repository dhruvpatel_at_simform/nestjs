import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserDto } from 'src/users/dto/CreateUser.dto';
import { User as UserEntity } from 'src/typeorm';
import { User, SerializedUser } from 'src/users/types/Users';
import { Repository } from 'typeorm';
import { generateSaltedPassword } from 'src/utils/bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  private users: User[] = [];

  getUsers() {
    return this.users.map((user) => new SerializedUser(user));
  }

  getUserByUsername(username: string): User {
    return this.users.find((user) => user.username === username);
  }

  getUserById(id: number): User {
    return this.users.find((user) => user.id === id);
  }

  createUser(createUserDto: CreateUserDto) {
    const password = generateSaltedPassword(createUserDto.password);
    const user = this.userRepository.create({ ...createUserDto, password });
    return this.userRepository.save(user);
  }

  findUserByUsername(username: string) {
    return this.userRepository.findOne({ username });
  }
}
